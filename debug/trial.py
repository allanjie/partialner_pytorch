# 
# @author: Allan
#

import argparse


def read_conf_file(file):
    f = open(file, 'r', encoding='utf-8')
    arguments = []
    for line in f:
        line = line.rstrip('\n').rstrip('\r')
        if line == "" or line.startswith('#'):
            continue
        arg, value = line.split(': ')
        arguments.append('--' + arg)
        arguments.append(value)
    f.close()
    return arguments


parser = argparse.ArgumentParser(description="Trial test for python utilities")


parser.add_argument('--word_embedding_dim', type=int, default=300, help='dimension of word-level lstm layer')
parser.add_argument('--mode', default='train', help='mode of the model')
parser.add_argument('--keep_labels', nargs='*', help='<Required> Set flag')

arguments = read_conf_file('./trial.yaml')
print(arguments)
args = parser.parse_args(arguments)

print(args.word_embedding_dim)
print(args.mode)
print(args.keep_labels)


x = args.keep_labels
print(len(x))
