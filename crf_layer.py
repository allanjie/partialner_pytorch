
import torch
import torch.nn as nn
import torch.nn.functional as F
from utils import *
from itertools import permutations


class CRF(nn.Module):

    '''
     @labels: size of tags, without the start and end.
     @enforce_constraint: enforce the iob constraint between labels
     @device: gpu/cpu
    '''
    def __init__(self, labels, enforce_constraint, device):
        super(CRF, self).__init__()
        print("CRF layer..")
        self.labels = labels
        self.labels.append(START_LABEL)
        self.labels.append(STOP_LABEL)
        self.label2idx = {c:i for i,c in enumerate(labels)}
        self.label_size = len(self.labels)

        self.transitions = nn.Parameter(
            torch.randn(self.label_size, self.label_size).to(device))
        self.transitions.data[self.label2idx[START_LABEL], :] = -10000
        self.transitions.data[:, self.label2idx[STOP_LABEL]] = -10000
        self.add_iob_constraint()
        self.transitions = self.transitions

    '''
    add the iob constraint to transition by make it low
    '''
    def add_iob_constraint(self):
        print(self.labels)
        for (l1, l2) in permutations(self.labels, 2):
            if l1
            self.transitions.data[self.label2idx[l1], :] = -10000
            print(l1 + " " + l2)

if __name__ == '__main__':
    print("something")
    labels = ["a" , "b"]
    device = torch.device("cuda")
    crf = CRF(labels, True, device)
