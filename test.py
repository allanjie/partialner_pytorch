# 
# @author: Allan
#


import torch

dtype = torch.float
device = torch.device("cpu")

sizeh = 10
sizey = 20
h = torch.randn(sizeh, 1, device=device, dtype=dtype)
y = torch.randn(sizey, 1, device=device, dtype=dtype)
w1 = torch.randn(sizey, sizeh, device=device, dtype=dtype, requires_grad=True)

learning_rate = 1e-6
for t in range(100000):

    pred_y = torch.mul(w1.mm(h), w1.mm(h)) + w1.mm(h)


    loss = (pred_y - y).pow(2).sum()
    print(loss.item())
    loss.backward()
    with torch.no_grad():
        w1 -= learning_rate * w1.grad
        #print(w1.grad)

        # Manually zero the gradients after updating weights
        w1.grad.zero_()



