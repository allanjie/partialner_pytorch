

class Span:
    def __init__(self, left, right, label):
        self.left = left
        self.right = right
        self.label = label

    def length(self):
        return self.right - self.left + 1

    def __eq__(self, other):
        return self.left == other.left and self.right == other.right and self.label == other.label

class Instance:
    def __init__(self, sent, spans):
        self.sent = sent
        self.spans = spans

def read_result(file):
    f = open(file , 'r', encoding='utf-8')
    meetSent = False
    insts=[]
    for line in f:
        line = line.rstrip('\n').rstrip('\r')
        if line == "":
            insts.append(Instance(sent, spans))
            meetSent = False
            continue
        if not meetSent:
            sent = line
            meetSent = True
            spans = []
        else:
            # print(line)
            _, left, right, label = line.split("\t")
            span = Span(int(left),int(right), label)
            spans.append(span)
    f.close()
    return insts


if __name__ == "__main__":
    gold_file = '../log/baseline_conll2003/testGoldEntities'
    pred_file = '../log/baseline_conll2003/testPredEntities'
    gold_insts = read_result(gold_file)
    pred_insts = read_result(pred_file)

    specific_length = 1

    total = 0
    corr = 0
    for g,p in zip(gold_insts, pred_insts):
        for gspan in g.spans:
            if gspan.length() == specific_length:
                total = total + 1
                if gspan in p.spans:
                    corr +=1
                else:
                    print(g.sent)
    print("corr: " + str(corr))
    print("total: " + str(total))
    print(" acc: {:.2f}".format(corr *1.0/total * 100) )