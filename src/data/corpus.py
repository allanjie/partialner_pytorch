from collections import Counter
from tqdm import tqdm
from src.data.token import Token
from src.data.utterance import Utterance
import os
import re

class Corpus:
    def __init__(self, path, useNormalizedWord):
        print(path)
        self.utterances, self.words, self.tags, self.chars = self.loadTrainData(path, useNormalizedWord) if "conll2003" in path else self.loadTrainDataNoPOS(path, useNormalizedWord)
        self.datasize = len(self.utterances)

    def loadTrainData(self, path, useNormalizedWord):
        words = Counter()
        tags = Counter()
        chars = Counter()
        utterances = []

        print("Start loading data...")
        with open(path, 'r', encoding='utf-8') as f:
            tokens = []
            for line in tqdm(f.readlines()):
                line = line.strip('\n')
                if line == "":
                    utterances.append(Utterance(tokens))
                    tokens = []
                    continue
                try:
                    word, pos, tag = line.split()
                    tags.update([tag])
                except:
                    word = line
                    tag = None
                if useNormalizedWord:
                    normalizedWord = re.sub('[1-9]', '0', word)
                else:
                    normalizedWord = word
                tokens.append(Token(normalizedWord, line, word, tag))
                words.update([normalizedWord])
                chars.update([c for c in normalizedWord])
        return utterances, words, tags, chars
##, errors='ignore'

    def loadTrainDataNoPOS(self, path, useNormalizedWord):
        words = Counter()
        tags = Counter()
        chars = Counter()
        utterances = []

        print("Start loading no pos data...")
        with open(path, 'r', encoding='utf-8') as f:
            tokens = []
            for line in tqdm(f.readlines()):
                line = line.strip('\n')
                if line == "":
                    utterances.append(Utterance(tokens))
                    tokens = []
                    continue
                try:
                    word, tag = line.split(" ")
                    tags.update([tag])
                except:
                    word = line
                    tag = None
                if useNormalizedWord:
                    normalizedWord = re.sub('[1-9]', '0', word)
                else:
                    normalizedWord = word
                tokens.append(Token(normalizedWord, line, word, tag))
                words.update([normalizedWord])
                chars.update([c for c in normalizedWord])
        return utterances, words, tags, chars