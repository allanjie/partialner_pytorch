from tqdm import tqdm
import random
import re
import numpy as np

class CorpusMeta:
    def __init__(self, corpus, Config):
        self.wordEmbedding = np.empty([len(corpus.words) + 2, Config.model.word_embedding_dim])
        self.useNormalizedWord = Config.data.use_normalized_word
        self.idx2Word = self.getIdx2Word(corpus.words, Config)
        self.word2idx = {self.idx2Word[idx]: idx for idx in range(len(self.idx2Word))}
        self.idx2Tag = self.getIdx2Tag(corpus.tags, Config)
        self.tag2Idx = {self.idx2Tag[idx]: idx for idx in range(len(self.idx2Tag))}
        self.idx2Char = self.getIdx2Char(corpus.chars, Config)
        self.char2Idx = {self.idx2Char[idx]: idx for idx in range(len(self.idx2Char))}
        self.unk = Config.data.UNK_ID
        self.charEmbeddingDim = self.getCharEmbeddingDim()

    def getCharEmbeddingDim(self):
        # return len(self.char2Idx) * 3
        return 30

    def getIdx2Word(self, words, Config):
        validWords = list(words.keys())
        validWords.insert(Config.data.PAD_ID, '<PAD>')
        validWords.insert(Config.data.UNK_ID, '<UNK>')
        scale = np.sqrt(3.0 / Config.model.word_embedding_dim)
        # for wordItem in words.most_common():
        #     if wordItem[1] <= Config.data.word_threshold:
        #         break
        #     validWords.append(wordItem[0])

        if Config.data.get('word_embedding', False):
            pretrainDict = {}
            with open(Config.data.word_embedding, 'r', encoding='utf-8') as fh:
                print('Reading word embeddings...')
                for line in tqdm(fh.readlines()):
                    items = line.split()
                    if len(items) == 2:
                        continue
                    # print(items[0] + " " + str(len(items)) + " " + str(Config.model.word_embedding_dim))
                    assert len(items) == Config.model.word_embedding_dim + 1
                    word = items[0]
                    if self.useNormalizedWord:
                        word = re.sub('[1-9]', '0', word)
                    if not word in pretrainDict:
                        embedding = np.empty([1, Config.model.word_embedding_dim])
                        embedding[:] = items[1:]
                        pretrainDict[word] = embedding
            cover = 0
            for wordIdx in range(len(validWords)):
                if validWords[wordIdx] in pretrainDict:
                    cover +=1
                    self.wordEmbedding[wordIdx, :] = pretrainDict[validWords[wordIdx]]
                elif validWords[wordIdx].lower() in pretrainDict:
                    cover +=1
                    self.wordEmbedding[wordIdx, :] = pretrainDict[validWords[wordIdx].lower()]
                else:
                    self.wordEmbedding[wordIdx, :] = np.random.uniform(-scale, scale,
                                                                       [1, Config.model.word_embedding_dim])
            print("word embedding found: " + str(cover) + " out of " +str(len(validWords)) + " coverage: " + str(cover * 1.0 / len(validWords)))
        else:
            for wordIdx in range(len(validWords)):
                self.wordEmbedding[wordIdx, :] = np.random.uniform(-scale, scale, [1, Config.model.word_embedding_dim])

        return validWords

    def getIdx2Tag(self, tags, Config):
        validTags =  [tag[0] for tag in tags.items()
                if len(Config.data.keep_labels) == 0 or tag[0] in Config.data.keep_labels]
        validTags.insert(Config.data.PAD_ID, '<PAD>')
        validTags.append('<START>')
        validTags.append('<EOS>')
        return validTags

    def getIdx2Char(self, chars, Config):
        validChars =  [char[0] for char in chars.items()]
        validChars.insert(Config.data.PAD_ID, '<PAD>')
        validChars.insert(Config.data.UNK_ID, '<UNK>')
        return validChars
