import torch
from torch import nn

class ElmoEmbedding(nn.Module):
    def __init__(self, layerUtil):
        super(ElmoEmbedding, self).__init__()
        self.embedding = layerUtil.getEmbeddingParameter()
        self.elmoLinear = layerUtil.getElmoLinear()
        self.dropout = layerUtil.getDropOut()
        self.linearAfterCat = layerUtil.getLinearAfterCat()

    def forward(self, seqTensor, elmoSeqTensor):
        if self.embedding is not None:
            wordSeqEmbedding = self.embedding(seqTensor)
        else:
            wordSeqEmbedding = None
        if self.elmoLinear is not None:
            newElmoSeqTensor = self.elmoLinear(elmoSeqTensor)
        else:
            newElmoSeqTensor = elmoSeqTensor
        if wordSeqEmbedding is not None:
            wordSeqEmbeddingElmo = torch.cat((wordSeqEmbedding, newElmoSeqTensor), 2)
        else:
            wordSeqEmbeddingElmo = newElmoSeqTensor
        if self.linearAfterCat is not None:
            newWordSeqEmbeddingElmo = self.linearAfterCat(wordSeqEmbeddingElmo)
        else:
            newWordSeqEmbeddingElmo = wordSeqEmbeddingElmo
        wordEmbeddingDropOut = self.dropout(newWordSeqEmbeddingElmo)
        return wordEmbeddingDropOut