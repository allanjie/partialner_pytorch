import torch
from torch import nn

class SimpleEmbedding(nn.Module):
    def __init__(self, layerUtil):
        super(SimpleEmbedding, self).__init__()
        self.embedding = layerUtil.getEmbeddingParameter()
        self.dropout = layerUtil.getDropOut()

    def forward(self, seqTensor):
        wordEmbedding = self.embedding(seqTensor)
        wordEmbeddingDropOut = self.dropout(wordEmbedding)
        return wordEmbeddingDropOut