import torch
from torch import nn
import torch.nn.functional as F

class CharCNN(nn.Module):
    def __init__(self, layerUtil):
        super(CharCNN, self).__init__()
        self.embedding = layerUtil.getCharEmbeddingParameter()
        self.conv1d = layerUtil.getCharConv()
        self.dropout = layerUtil.getDropOut()

    def forward(self, seqTensors, seqLengths):
        batchSize = seqTensors.shape[0]
        sentLength = seqTensors.shape[1]
        seqTensors = seqTensors.view(batchSize * sentLength, -1)
        # seqLengths = seqLengths.view(batchSize * sentLength)
        # sortedSeqLengths, permIdx = seqLengths.sort(0, descending=True)
        # _, recoverPermIdx = permIdx.sort(0, descending=False)
        # sortedSeqTensors = seqTensors[permIdx]
        # sortedSeqEmbedding = self.dropout(self.embedding(sortedSeqTensors)).transpose(2, 1)
        # sortedSeqCNNOut = self.conv1d(sortedSeqEmbedding)
        # sortedSeqPoolOut = F.max_pool1d(sortedSeqCNNOut, sortedSeqCNNOut.size(2))
        #
        # return sortedSeqPoolOut[recoverPermIdx].view(batchSize, sentLength, -1)
        SeqEmbedding = self.dropout(self.embedding(seqTensors)).transpose(2, 1)
        SeqCNNOut = self.conv1d(SeqEmbedding)
        SeqPoolOut = F.max_pool1d(SeqCNNOut, SeqCNNOut.size(2))

        return SeqPoolOut.view(batchSize, sentLength, -1)