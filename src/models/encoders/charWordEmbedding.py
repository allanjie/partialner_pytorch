import torch
from torch import nn
from src.models.encoders.charCNN import CharCNN
from src.models.encoders.charRNN import CharRNN

class CharWordEmbedding(nn.Module):
    def __init__(self, layerUtil):
        super(CharWordEmbedding, self).__init__()
        self.embedding = layerUtil.getEmbeddingParameter()
        # self.charEmbedding = CharCNN(layerUtil)
        self.charEmbedding = CharRNN(layerUtil)
        self.dropout = layerUtil.getDropOut()

    def forward(self, wordSeqTensors, charSeqTensors, charSeqLengths):
        wordEmbedding = self.embedding(wordSeqTensors)
        charCNNEmbedding = self.charEmbedding(charSeqTensors, charSeqLengths)
        mergeEmbedding = torch.cat([wordEmbedding, charCNNEmbedding], 2)
        wordEmbeddingDropOut = self.dropout(mergeEmbedding)
        return wordEmbeddingDropOut