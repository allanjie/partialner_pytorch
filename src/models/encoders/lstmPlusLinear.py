import torch
from torch import nn
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence

class LstmPlusLinear(nn.Module):
    def __init__(self, layerUtils):
        super(LstmPlusLinear, self).__init__()
        self.rnn, self.initHiddenWithBatchSize = layerUtils.getEncoderRNN()
        self.linear = layerUtils.getEncoderLinear()
        self.dropout = layerUtils.getDropOut()

    def forward(self, inputs, inputSeqLengths):
        packedWords = pack_padded_sequence(inputs, inputSeqLengths, True)
        hidden = self.initHiddenWithBatchSize(len(inputs))
        lstmOut, hidden = self.rnn(packedWords, hidden)
        lstmOut, _ = pad_packed_sequence(lstmOut, batch_first=True)
        linearInputDropout = self.dropout(lstmOut)
        linearOutput = self.linear(linearInputDropout)

        return linearOutput
