import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.autograd as autograd
import torch.optim as optim
from tqdm import tqdm
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
import re
import numpy as np
import math


class BaselineDict(nn.Module):
    def __init__(self, Config, layerHelper):
        super(BaselineDict, self).__init__()
        self.wordEmbedding = layerHelper.getWordEmbedding()
        self.encoder = layerHelper.getEncoder()
        self.crf = layerHelper.getCRF()
        self.useGpu = Config.use_gpu
        self.dict = self.readDic()
        self.dictTrain = self.readDicTrain()

    def readDic(self):
        labelType = ["A", "D", "M", "O", "S"]
        d = {}
        for label in labelType:
            d[label] = {}
            with open('dict/' + label + '.dic', 'r', encoding='utf8') as fh:
                for line in fh.readlines():
                    line = line.strip()
                    if line == '':
                        continue
                    entity, occ = line.split('\t')
                    d[label][entity] = float(occ)
            occs = np.asarray(list(d[label].values()))
            medi = np.median(occs)
            for entity in d[label]:
                d[label][entity] = 1 / (1 + math.exp(-(d[label][entity] - medi)))

        return d

    def readDicTrain(self):
        labelType = ["A", "D", "M", "O", "S"]
        d = {}
        for label in labelType:
            d[label] = {}
            with open('dict_qiuewei/' + label + '.dic', 'r', encoding='utf8') as fh:
                for line in fh.readlines():
                    line = line.strip()
                    if line == '':
                        continue
                    entity, occ = line.split('\t')
                    d[label][entity] = float(occ)
            occs = np.asarray(list(d[label].values()))
            medi = np.median(occs)
            for entity in d[label]:
                d[label][entity] = 1 / (1 + math.exp(-(d[label][entity] - medi)))

        return d

    def getTags(self, entity, label):
        if len(entity) == 1:
            return ['S-' + label]
        tags = ['B-' + label]
        tags += ['I-' + label] * (len(entity) - 2)
        tags += ['E-' + label]
        return tags

    def addStartScores(self, tag2Idx, tag, occ):
        labelType = ["A", "D", "M", "O", "S"]
        scores = torch.zeros([len(tag2Idx), len(tag2Idx)])
        scores[tag2Idx[tag],tag2Idx['O']] += occ
        for label in labelType:
            if 'S-' + label in tag2Idx:
                scores[tag2Idx[tag], tag2Idx['S-' + label]] += occ
            if 'E-' + label in tag2Idx:
                scores[tag2Idx[tag], tag2Idx['E-' + label]] += occ
        return scores

    def addEndScores(self, tag2Idx, tag, occ):
        labelType = ["A", "D", "M", "O", "S"]
        scores = torch.zeros([len(tag2Idx), len(tag2Idx)])
        scores[tag2Idx['O'], tag2Idx[tag]] += occ
        for label in labelType:
            if 'S-' + label in tag2Idx:
                scores[tag2Idx['S-' + label], tag2Idx[tag]] += occ
            if 'B-' + label in tag2Idx:
                scores[tag2Idx['B-' + label], tag2Idx[tag]] += occ
        return scores

    def getDicScores(self, tokens, scores, tag2Idx):
        sentence = ''.join([token.rawWord for token in tokens])
        for label in self.dict:
            for entity in self.dict[label]:
                try:
                    mat = []
                    pos = sentence.find(entity)
                    while pos != -1:
                        mat.append(pos)
                        pos = sentence.find(entity, pos + 1)
                except:
                    print(entity)
                    print(sentence)
                    exit(1)
                if len(mat) > 0:
                    tags = self.getTags(entity, label)
                    flag = False
                    for tag in tags:
                        if not tag in tag2Idx:
                            flag = True
                            break
                    if flag:
                        continue
                    occ = self.dict[label][entity]
                    for start in mat:
                        end = start + len(entity)
                        scores[start, :, :] += self.addStartScores(tag2Idx, tags[0], occ)
                        if end < len(scores):
                            scores[end, :, :] += self.addEndScores(tag2Idx, tags[-1], occ)
                        for inter in range(1, len(entity)):
                            scores[start + inter, tag2Idx[tags[inter - 1]], tag2Idx[tags[inter]]] += occ
        return torch.log(1 + scores)

    def getDicTrainScores(self, tokens, scores, tag2Idx):
        sentence = ''.join([token.rawWord for token in tokens])
        for label in self.dictTrain:
            for entity in self.dictTrain[label]:
                try:
                    mat = []
                    pos = sentence.find(entity)
                    while pos != -1:
                        mat.append(pos)
                        pos = sentence.find(entity, pos + 1)
                except:
                    print(entity)
                    print(sentence)
                    exit(1)
                if len(mat) > 0:
                    tags = self.getTags(entity, label)
                    flag = False
                    for tag in tags:
                        if not tag in tag2Idx:
                            flag = True
                            break
                    if flag:
                        continue
                    occ = self.dictTrain[label][entity]
                    for start in mat:
                        end = start + len(entity)
                        scores[start, :, :] += self.addStartScores(tag2Idx, tags[0], occ)
                        if end < len(scores):
                            scores[end, :, :] += self.addEndScores(tag2Idx, tags[-1], occ)
                        for inter in range(1, len(entity)):
                            scores[start + inter, tag2Idx[tags[inter - 1]], tag2Idx[tags[inter]]] += occ
        return torch.log(1 + scores)


    def generateBatchInput(self, corpus, corpusMeta, batchSize):
        word2Idx = corpusMeta.word2idx
        tag2Idx = corpusMeta.tag2Idx
        inputBatches = []
        totalSize = len(corpus.utterances)
        for batchId in tqdm(range(totalSize // batchSize)):
            batchUtts = corpus.utterances[batchId * batchSize: (batchId + 1) * batchSize]
            batchUtts = sorted(batchUtts, key=lambda utt: len(utt.tokens), reverse=True)
            wordSeqLengths = torch.LongTensor(list(map(lambda utt: len(utt.tokens), batchUtts)))
            maxSeqLength = wordSeqLengths.max()
            wordSeqTensor = autograd.Variable(torch.zeros((batchSize, maxSeqLength))).long()
            tagSeqTensor = autograd.Variable(torch.zeros((batchSize, maxSeqLength))).long()
            dicScores = torch.zeros([batchSize, maxSeqLength, len(tag2Idx), len(tag2Idx)])
            dicTrainScores = torch.zeros([batchSize, maxSeqLength, len(tag2Idx), len(tag2Idx)])
            for idx in range(batchSize):
                wordSeqTensor[idx, :wordSeqLengths[idx]] = torch.LongTensor([word2Idx.get(word.text, corpusMeta.unk)for word in batchUtts[idx].tokens])
                tagSeqTensor[idx, :wordSeqLengths[idx]] = torch.LongTensor([tag2Idx.get(word.tag, corpusMeta.unk) for word in batchUtts[idx].tokens])
                dicScores[idx, :wordSeqLengths[idx], :, :] = self.getDicScores(batchUtts[idx].tokens, torch.zeros([wordSeqLengths[idx], len(tag2Idx), len(tag2Idx)]), tag2Idx)
                dicTrainScores[idx, :wordSeqLengths[idx], :, :] = self.getDicTrainScores(batchUtts[idx].tokens, torch.zeros(
                    [wordSeqLengths[idx], len(tag2Idx), len(tag2Idx)]), tag2Idx)
            if self.useGpu:
                wordSeqTensor = wordSeqTensor.cuda()
                tagSeqTensor = tagSeqTensor.cuda()
                wordSeqLengths = wordSeqLengths.cuda()
                dicScores = dicScores.cuda()
                dicTrainScores = dicTrainScores.cuda()
            inputBatches.append((wordSeqTensor, tagSeqTensor, wordSeqLengths, dicScores))
        if len(inputBatches) * batchSize < totalSize:
            startId = len(inputBatches) * batchSize
            lastBatchSize = totalSize - startId
            batchUtts = corpus.utterances[startId: totalSize]
            batchUtts = sorted(batchUtts, key=lambda utt: len(utt.tokens), reverse=True)
            wordSeqLengths = torch.LongTensor(list(map(lambda utt: len(utt.tokens), batchUtts)))
            maxSeqLength = wordSeqLengths.max()
            wordSeqTensor = autograd.Variable(torch.zeros((lastBatchSize, maxSeqLength))).long()
            tagSeqTensor = autograd.Variable(torch.zeros((lastBatchSize, maxSeqLength))).long()
            dicScores = torch.zeros([lastBatchSize, maxSeqLength, len(tag2Idx), len(tag2Idx)])
            dicTrainScores = torch.zeros([lastBatchSize, maxSeqLength, len(tag2Idx), len(tag2Idx)])
            for idx in range(lastBatchSize):
                wordSeqTensor[idx, :wordSeqLengths[idx]] = torch.LongTensor(
                    [word2Idx.get(word.text, corpusMeta.unk) for word in batchUtts[idx].tokens])
                tagSeqTensor[idx, :wordSeqLengths[idx]] = torch.LongTensor(
                    [tag2Idx.get(word.tag) for word in batchUtts[idx].tokens])
                dicScores[idx, :wordSeqLengths[idx], :, :] = self.getDicScores(batchUtts[idx].tokens, torch.zeros(
                    [wordSeqLengths[idx], len(tag2Idx), len(tag2Idx)]), tag2Idx)
                dicTrainScores[idx, :wordSeqLengths[idx], :, :] = self.getDicTrainScores(batchUtts[idx].tokens, torch.zeros(
                    [wordSeqLengths[idx], len(tag2Idx), len(tag2Idx)]), tag2Idx)
            if self.useGpu:
                wordSeqTensor = wordSeqTensor.cuda()
                tagSeqTensor = tagSeqTensor.cuda()
                wordSeqLengths = wordSeqLengths.cuda()
                dicScores = dicScores.cuda()
            inputBatches.append((wordSeqTensor, tagSeqTensor, wordSeqLengths, dicScores, dicTrainScores))

        return inputBatches

    def getRawSentenceBatches(self, corpus, corpusMeta, batchSize):
        rawSentenceBatches = []
        totalSize = len(corpus.utterances)
        for batchId in range(totalSize // batchSize):
            batchUtts = corpus.utterances[batchId * batchSize: (batchId + 1) * batchSize]
            batchUtts = sorted(batchUtts, key=lambda utt: len(utt.tokens), reverse=True)
            sentences = []
            for idx in range(batchSize):
                sentences.append([word.rawWord for word in batchUtts[idx].tokens])
            rawSentenceBatches.append(sentences)
        if len(rawSentenceBatches) * batchSize < totalSize:
            startId = len(rawSentenceBatches) * batchSize
            lastBatchSize = totalSize - startId
            batchUtts = corpus.utterances[startId: totalSize]
            batchUtts = sorted(batchUtts, key=lambda utt: len(utt.tokens), reverse=True)
            sentences = []
            for idx in range(lastBatchSize):
                sentences.append([word.rawWord for word in batchUtts[idx].tokens])
            rawSentenceBatches.append(sentences)
        return rawSentenceBatches


    def negLogLikelihoodLoss(self, batchInput):
        wordSeqTensor, tagSeqTensor, wordSeqLengths, dicScores, dicTrainScores = batchInput
        batchSize = wordSeqTensor.shape[0]
        sentLength = wordSeqTensor.shape[1]
        maskTemp = torch.range(1, sentLength, dtype=torch.int64).view(1, sentLength).expand(batchSize, sentLength)
        if self.useGpu:
            maskTemp = maskTemp.cuda()
        mask = torch.le(maskTemp, wordSeqLengths.view(batchSize, 1).expand(batchSize, sentLength))
        if self.useGpu:
            mask = mask.cuda()
        wordSeqEmbedding = self.wordEmbedding(wordSeqTensor)
        wordFeatures = self.encoder(wordSeqEmbedding, wordSeqLengths, True)
        totalScore, scores = self.crf(wordFeatures, wordSeqLengths, mask, dicTrainScores)
        goldScore = self.crf.scoreSentence(tagSeqTensor, wordSeqLengths, scores, mask)

        return totalScore - goldScore

    def forward(self, batchInput):
        wordSeqTensor, tagSeqTensor, wordSeqLengths, dicScores, dicTrainScores = batchInput
        wordSeqEmbedding = self.wordEmbedding(wordSeqTensor)
        wordFeatures = self.encoder(wordSeqEmbedding, wordSeqLengths, False)
        bestScores, decodeIdx = self.crf.viterbiDecode(wordFeatures, wordSeqLengths, dicScores + dicTrainScores)
        return bestScores, decodeIdx