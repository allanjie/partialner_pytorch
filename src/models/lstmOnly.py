import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.autograd as autograd
import torch.optim as optim
from tqdm import tqdm
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence


class LSTMOnly(nn.Module):
    def __init__(self, Config, wordSize, tagSize, preEmb):
        super(LSTMOnly, self).__init__()
        self.Config = Config
        self.useGpu = Config.use_gpu
        embeddingDim = Config.model.word_embedding_dim
        dropout = Config.model.drop_out
        hiddenDim = Config.model.rnn_hidden_size
        # self.wordEmbedding = nn.Embedding.from_pretrained(torch.FloatTensor(preEmb), freeze=False)
        self.wordEmbedding = nn.Embedding(wordSize, embeddingDim)
        self.wordEmbedding.weight.data.copy_(torch.FloatTensor(preEmb))
        self.wordDropOut = nn.Dropout(dropout)
        self.lstm = nn.LSTM(embeddingDim, hiddenDim // 2, Config.model.rnn_num_layers, bidirectional=True)
        self.lstmDropOut = nn.Dropout(dropout)
        self.linear = nn.Linear(hiddenDim, tagSize)
        if self.useGpu:
            self.wordEmbedding = self.wordEmbedding.cuda()
            self.wordDropOut = self.wordDropOut.cuda()
            self.lstm = self.lstm.cuda()
            self.lstmDropOut = self.lstmDropOut.cuda()
            self.linear = self.linear.cuda()


    def generateBatchInput(self, corpus, corpusMeta, batchSize):
        word2Idx = corpusMeta.word2idx
        tag2Idx = corpusMeta.tag2Idx
        inputBatches = []
        totalSize = len(corpus.utterances)
        for batchId in tqdm(range(totalSize // batchSize)):
            batchUtts = corpus.utterances[batchId * batchSize: (batchId + 1) * batchSize]
            wordSeqLengths = torch.LongTensor(list(map(lambda utt: len(utt.tokens), batchUtts)))
            # batchUtts = sorted(batchUtts, key=lambda utt: len(utt.tokens), reverse=True)
            maxSeqLength = wordSeqLengths.max()
            wordSeqTensor = autograd.Variable(torch.zeros((batchSize, maxSeqLength))).long()
            tagSeqTensor = autograd.Variable(torch.zeros((batchSize, maxSeqLength))).long()
            for idx in range(batchSize):
                wordSeqTensor[idx, :wordSeqLengths[idx]] = torch.LongTensor(
                    [word2Idx.get(word.text, corpusMeta.unk) for word in batchUtts[idx].tokens])
                tagSeqTensor[idx, :wordSeqLengths[idx]] = torch.LongTensor(
                    [tag2Idx[word.tag] for word in batchUtts[idx].tokens])
            if self.useGpu:
                wordSeqTensor = wordSeqTensor.cuda()
                tagSeqTensor = tagSeqTensor.cuda()
                wordSeqLengths = wordSeqLengths.cuda()
            wordSeqLengths, permIdx = wordSeqLengths.sort(0, descending=True)
            inputBatches.append((wordSeqTensor[permIdx], tagSeqTensor[permIdx], wordSeqLengths))
        if len(inputBatches) * batchSize < totalSize:
            startId = len(inputBatches) * batchSize
            lastBatchSize = totalSize - startId
            batchUtts = corpus.utterances[startId: totalSize]
            # batchUtts = sorted(batchUtts, key=lambda utt: len(utt.tokens), reverse=True)
            wordSeqLengths = torch.LongTensor(list(map(lambda utt: len(utt.tokens), batchUtts)))
            maxSeqLength = wordSeqLengths.max()
            wordSeqTensor = autograd.Variable(torch.zeros((lastBatchSize, maxSeqLength))).long()
            tagSeqTensor = autograd.Variable(torch.zeros((lastBatchSize, maxSeqLength))).long()
            for idx in range(lastBatchSize):
                wordSeqTensor[idx, :wordSeqLengths[idx]] = torch.LongTensor(
                    [word2Idx.get(word.text, corpusMeta.unk) for word in batchUtts[idx].tokens])
                tagSeqTensor[idx, :wordSeqLengths[idx]] = torch.LongTensor(
                    [tag2Idx[word.tag] for word in batchUtts[idx].tokens])
            if self.useGpu:
                wordSeqTensor = wordSeqTensor.cuda()
                tagSeqTensor = tagSeqTensor.cuda()
                wordSeqLengths = wordSeqLengths.cuda()
            wordSeqLengths, permIdx = wordSeqLengths.sort(0, descending=True)
            inputBatches.append((wordSeqTensor[permIdx], tagSeqTensor[permIdx], wordSeqLengths))

        return inputBatches

    def getRawSentenceBatches(self, corpus, corpusMeta, batchSize):
        rawSentenceBatches = []
        totalSize = len(corpus.utterances)
        for batchId in range(totalSize // batchSize):
            batchUtts = corpus.utterances[batchId * batchSize: (batchId + 1) * batchSize]
            batchUtts = sorted(batchUtts, key=lambda utt: len(utt.tokens), reverse=True)
            sentences = []
            wordSeqLengths = torch.LongTensor(list(map(lambda utt: len(utt.tokens), batchUtts)))
            wordSeqLengths, permIdx = wordSeqLengths.sort(0, descending=True)
            for idx in permIdx:
                sentences.append([word.rawWord for word in batchUtts[idx].tokens])
            rawSentenceBatches.append(sentences)
        if len(rawSentenceBatches) * batchSize < totalSize:
            startId = len(rawSentenceBatches) * batchSize
            lastBatchSize = totalSize - startId
            batchUtts = corpus.utterances[startId: totalSize]
            batchUtts = sorted(batchUtts, key=lambda utt: len(utt.tokens), reverse=True)
            sentences = []
            wordSeqLengths = torch.LongTensor(list(map(lambda utt: len(utt.tokens), batchUtts)))
            wordSeqLengths, permIdx = wordSeqLengths.sort(0, descending=True)
            for idx in permIdx:
                sentences.append([word.rawWord for word in batchUtts[idx].tokens])
            rawSentenceBatches.append(sentences)
        return rawSentenceBatches

    def negLogLikelihoodLoss(self, batchInput):
        wordSeqTensor, tagSeqTensor, wordSeqLengths = batchInput
        # with open('myinput', 'a', encoding='utf-8') as fh:
        #     fh.write(str(wordSeqTensor.data.tolist()) + '\n')
        #     fh.write(str(tagSeqTensor.data.tolist()) + '\n')
        #     fh.write(str(wordSeqLengths.tolist()) + '\n===================================\n')
            
        batchSize = wordSeqTensor.shape[0]
        sentLength = wordSeqTensor.shape[1]
        wordSeqEmbedding = self.wordDropOut(self.wordEmbedding(wordSeqTensor))

        packedWords = pack_padded_sequence(wordSeqEmbedding, wordSeqLengths.cpu().numpy(), batch_first=True)
        lstmOutTmp, _ = self.lstm(packedWords, None)
        lstmOut, _ = pad_packed_sequence(lstmOutTmp, batch_first=True)
        linearInputDropout = self.lstmDropOut(lstmOut)
        outs = self.linear(linearInputDropout)

        loss_function = nn.NLLLoss(ignore_index=0, size_average=False)
        outs = outs.view(batchSize * sentLength, -1)
        score = F.log_softmax(outs, 1)
        total_loss = loss_function(score, tagSeqTensor.view(batchSize * sentLength))
        _, tag_seq = torch.max(score, 1)
        tag_seq = tag_seq.view(batchSize, sentLength)

        return total_loss

        # m = nn.LogSoftmax(dim=2)
        # loss = nn.NLLLoss(ignore_index=0, size_average=False)
        # return loss(m(wordFeatures).view(batchSize*sentLength, -1), tagSeqTensor.view(batchSize*sentLength))

    def forward(self, batchInput):
        wordSeqTensor, tagSeqTensor, wordSeqLengths = batchInput
        wordSeqEmbedding = self.wordDropOut(self.wordEmbedding(wordSeqTensor))

        packedWords = pack_padded_sequence(wordSeqEmbedding, wordSeqLengths.cpu().numpy(), True)
        lstmOutTmp, _ = self.lstm(packedWords, None)
        lstmOut, _ = pad_packed_sequence(lstmOutTmp, batch_first=True)
        linearInputDropout = self.lstmDropOut(lstmOut)
        wordFeatures = self.linear(linearInputDropout)
        _, decodeIdx = torch.max(wordFeatures, 2)
        return 0, decodeIdx.view(wordFeatures.shape[0], wordFeatures.shape[1])
