import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.autograd as autograd
import torch.optim as optim
from tqdm import tqdm
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence


class Baseline(nn.Module):
    def __init__(self, Config, layerHelper):
        super(Baseline, self).__init__()
        self.wordEmbedding = layerHelper.getWordEmbedding()
        self.encoder = layerHelper.getEncoder()
        self.crf = layerHelper.getCRF()
        self.useGpu = Config.use_gpu
        self.useChar = Config.model.use_char

    def generateBatchInput(self, corpus, corpusMeta, batchSize):
        word2Idx = corpusMeta.word2idx
        tag2Idx = corpusMeta.tag2Idx
        char2Idx = corpusMeta.char2Idx
        inputBatches = []
        totalSize = len(corpus.utterances)
        for batchId in tqdm(range(totalSize // batchSize)):
            batchUtts = corpus.utterances[batchId * batchSize: (batchId + 1) * batchSize]
            batchUtts = sorted(batchUtts, key=lambda utt: len(utt.tokens), reverse=True)
            wordSeqLengths = torch.LongTensor(list(map(lambda utt: len(utt.tokens), batchUtts)))
            maxSeqLength = wordSeqLengths.max()
            charSeqLengths = torch.LongTensor([list(map(lambda tok: len(tok.chars), utt.tokens)) + [1] * (int(maxSeqLength) - len(utt.tokens)) for utt in batchUtts])
            maxCharLength = charSeqLengths.max()
            wordSeqTensor = autograd.Variable(torch.zeros((batchSize, maxSeqLength))).long()
            tagSeqTensor = autograd.Variable(torch.zeros((batchSize, maxSeqLength))).long()
            charSeqTensor = autograd.Variable(torch.zeros((batchSize, maxSeqLength, maxCharLength))).long()
            for idx in range(batchSize):
                wordSeqTensor[idx, :wordSeqLengths[idx]] = torch.LongTensor([word2Idx.get(word.text, corpusMeta.unk)for word in batchUtts[idx].tokens])
                tagSeqTensor[idx, :wordSeqLengths[idx]] = torch.LongTensor([tag2Idx[word.tag] for word in batchUtts[idx].tokens])
                for wordIdx in range(wordSeqLengths[idx]):
                    charSeqTensor[idx, wordIdx, :charSeqLengths[idx, wordIdx]] = torch.LongTensor([char2Idx.get(char, corpusMeta.unk) for char in batchUtts[idx].tokens[wordIdx].chars])
                for wordIdx in range(wordSeqLengths[idx], maxSeqLength):
                    charSeqTensor[idx, wordIdx, 0: 1] = torch.LongTensor([char2Idx['<PAD>']])
            if self.useGpu:
                wordSeqTensor = wordSeqTensor.cuda()
                tagSeqTensor = tagSeqTensor.cuda()
                wordSeqLengths = wordSeqLengths.cuda()
                charSeqTensor = charSeqTensor.cuda()
                charSeqLengths = charSeqLengths.cuda()
            inputBatches.append((wordSeqTensor, tagSeqTensor, wordSeqLengths, charSeqTensor, charSeqLengths))
        if len(inputBatches) * batchSize < totalSize:
            startId = len(inputBatches) * batchSize
            lastBatchSize = totalSize - startId
            batchUtts = corpus.utterances[startId: totalSize]
            batchUtts = sorted(batchUtts, key=lambda utt: len(utt.tokens), reverse=True)
            wordSeqLengths = torch.LongTensor(list(map(lambda utt: len(utt.tokens), batchUtts)))
            maxSeqLength = wordSeqLengths.max()
            charSeqLengths = torch.LongTensor(
                [list(map(lambda tok: len(tok.chars), utt.tokens)) + [1] * (int(maxSeqLength) - len(utt.tokens)) for utt in
                 batchUtts])
            maxCharLength = charSeqLengths.max()
            wordSeqTensor = autograd.Variable(torch.zeros((lastBatchSize, maxSeqLength))).long()
            tagSeqTensor = autograd.Variable(torch.zeros((lastBatchSize, maxSeqLength))).long()
            charSeqTensor = autograd.Variable(torch.zeros((lastBatchSize, maxSeqLength, maxCharLength))).long()
            for idx in range(lastBatchSize):
                wordSeqTensor[idx, :wordSeqLengths[idx]] = torch.LongTensor(
                    [word2Idx.get(word.text, corpusMeta.unk) for word in batchUtts[idx].tokens])
                tagSeqTensor[idx, :wordSeqLengths[idx]] = torch.LongTensor(
                    [tag2Idx[word.tag] for word in batchUtts[idx].tokens])
                for wordIdx in range(wordSeqLengths[idx]):
                    charSeqTensor[idx, wordIdx, :charSeqLengths[idx, wordIdx]] = torch.LongTensor([char2Idx.get(char, corpusMeta.unk) for char in batchUtts[idx].tokens[wordIdx].chars])
                for wordIdx in range(wordSeqLengths[idx] + 1, maxSeqLength):
                    charSeqTensor[idx, wordIdx, 0] = torch.LongTensor([char2Idx['<PAD>']])
            if self.useGpu:
                wordSeqTensor = wordSeqTensor.cuda()
                tagSeqTensor = tagSeqTensor.cuda()
                wordSeqLengths = wordSeqLengths.cuda()
                charSeqTensor = charSeqTensor.cuda()
                charSeqLengths = charSeqLengths.cuda()
            inputBatches.append((wordSeqTensor, tagSeqTensor, wordSeqLengths, charSeqTensor, charSeqLengths))

        return inputBatches

    def getRawSentenceBatches(self, corpus, corpusMeta, batchSize):
        rawSentenceBatches = []
        totalSize = len(corpus.utterances)
        for batchId in range(totalSize // batchSize):
            batchUtts = corpus.utterances[batchId * batchSize: (batchId + 1) * batchSize]
            batchUtts = sorted(batchUtts, key=lambda utt: len(utt.tokens), reverse=True)
            sentences = []
            for idx in range(batchSize):
                sentences.append([word.rawWord for word in batchUtts[idx].tokens])
            rawSentenceBatches.append(sentences)
        if len(rawSentenceBatches) * batchSize < totalSize:
            startId = len(rawSentenceBatches) * batchSize
            lastBatchSize = totalSize - startId
            batchUtts = corpus.utterances[startId: totalSize]
            batchUtts = sorted(batchUtts, key=lambda utt: len(utt.tokens), reverse=True)
            sentences = []
            for idx in range(lastBatchSize):
                sentences.append([word.rawWord for word in batchUtts[idx].tokens])
            rawSentenceBatches.append(sentences)
        return rawSentenceBatches


    def negLogLikelihoodLoss(self, batchInput):
        wordSeqTensor, tagSeqTensor, wordSeqLengths, charSeqTensor, charSeqLengths = batchInput
        batchSize = wordSeqTensor.shape[0]
        sentLength = wordSeqTensor.shape[1]
        maskTemp = torch.range(1, sentLength, dtype=torch.int64).view(1, sentLength).expand(batchSize, sentLength)
        if self.useGpu:
            maskTemp = maskTemp.cuda()
        mask = torch.le(maskTemp, wordSeqLengths.view(batchSize, 1).expand(batchSize, sentLength))
        if self.useGpu:
            mask = mask.cuda()
        if self.useChar:
            wordSeqEmbedding = self.wordEmbedding(wordSeqTensor, charSeqTensor, charSeqLengths)
        else:
            wordSeqEmbedding = self.wordEmbedding(wordSeqTensor)
        wordFeatures = self.encoder(wordSeqEmbedding, wordSeqLengths)
        totalScore, scores = self.crf(wordFeatures, wordSeqLengths, mask)
        goldScore = self.crf.scoreSentence(tagSeqTensor, wordSeqLengths, scores, mask)
        
        return totalScore - goldScore
        #m = nn.LogSoftmax(dim=2)
        #loss = nn.NLLLoss(ignore_index=0, size_average=False)
        #return loss(m(wordFeatures).view(batchSize*sentLength, -1), tagSeqTensor.view(batchSize*sentLength))
        

    def forward(self, batchInput):
        wordSeqTensor, tagSeqTensor, wordSeqLengths, charSeqTensor, charSeqLengths = batchInput
        if self.useChar:
            wordSeqEmbedding = self.wordEmbedding(wordSeqTensor, charSeqTensor, charSeqLengths)
        else:
            wordSeqEmbedding = self.wordEmbedding(wordSeqTensor)
        wordFeatures = self.encoder(wordSeqEmbedding, wordSeqLengths)
        bestScores, decodeIdx = self.crf.viterbiDecode(wordFeatures, wordSeqLengths)
        return bestScores, decodeIdx
        #decodeIdx = torch.argmax(wordFeatures, 2)
        #return 0, decodeIdx.view(wordFeatures.shape[0], wordFeatures.shape[1])
