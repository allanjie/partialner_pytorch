import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.autograd as autograd
import torch.optim as optim
from tqdm import tqdm
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence


class BaselineElmo(nn.Module):
    def __init__(self, Config, layerHelper):
        super(BaselineElmo, self).__init__()
        self.wordEmbedding = layerHelper.getWordEmbedding()
        self.encoder = layerHelper.getEncoder()
        self.crf = layerHelper.getCRF()
        self.useGpu = Config.use_gpu
        self.elmoDim = Config.model.elmo_dim

    def generateBatchInput(self, corpus, corpusMeta, batchSize):
        word2Idx = corpusMeta.word2idx
        tag2Idx = corpusMeta.tag2Idx
        inputBatches = []
        totalSize = len(corpus.utterances)
        for batchId in tqdm(range(totalSize // batchSize)):
            batchUtts = corpus.utterances[batchId * batchSize: (batchId + 1) * batchSize]
            batchUtts = sorted(batchUtts, key=lambda utt: len(utt.tokens), reverse=True)
            wordSeqLengths = torch.LongTensor(list(map(lambda utt: len(utt.tokens), batchUtts)))
            maxSeqLength = wordSeqLengths.max()
            wordSeqTensor = autograd.Variable(torch.zeros((batchSize, maxSeqLength))).long()
            tagSeqTensor = autograd.Variable(torch.zeros((batchSize, maxSeqLength))).long()
            elmoSeqTensor = autograd.Variable(torch.zeros([batchSize, maxSeqLength, self.elmoDim]))
            for idx in range(batchSize):
                wordSeqTensor[idx, :wordSeqLengths[idx]] = torch.LongTensor([word2Idx.get(word.text, corpusMeta.unk)for word in batchUtts[idx].tokens])
                tagSeqTensor[idx, :wordSeqLengths[idx]] = torch.LongTensor([tag2Idx.get(word.tag, corpusMeta.unk) for word in batchUtts[idx].tokens])
                elmoSeqTensor[idx, :wordSeqLengths[idx]] = torch.Tensor([word.elmo[: self.elmoDim] for word in batchUtts[idx].tokens])
            if self.useGpu:
                wordSeqTensor = wordSeqTensor.cuda()
                tagSeqTensor = tagSeqTensor.cuda()
                wordSeqLengths = wordSeqLengths.cuda()
                elmoSeqTensor = elmoSeqTensor.cuda()
            inputBatches.append((wordSeqTensor, tagSeqTensor, wordSeqLengths, elmoSeqTensor))
        if len(inputBatches) * batchSize < totalSize:
            startId = len(inputBatches) * batchSize
            lastBatchSize = totalSize - startId
            batchUtts = corpus.utterances[startId: totalSize]
            batchUtts = sorted(batchUtts, key=lambda utt: len(utt.tokens), reverse=True)
            wordSeqLengths = torch.LongTensor(list(map(lambda utt: len(utt.tokens), batchUtts)))
            maxSeqLength = wordSeqLengths.max()
            wordSeqTensor = autograd.Variable(torch.zeros((lastBatchSize, maxSeqLength))).long()
            tagSeqTensor = autograd.Variable(torch.zeros((lastBatchSize, maxSeqLength))).long()
            elmoSeqTensor = autograd.Variable(torch.zeros([lastBatchSize, maxSeqLength, self.elmoDim]))
            for idx in range(lastBatchSize):
                wordSeqTensor[idx, :wordSeqLengths[idx]] = torch.LongTensor(
                    [word2Idx.get(word.text, corpusMeta.unk) for word in batchUtts[idx].tokens])
                tagSeqTensor[idx, :wordSeqLengths[idx]] = torch.LongTensor(
                    [tag2Idx.get(word.tag) for word in batchUtts[idx].tokens])
                elmoSeqTensor[idx, :wordSeqLengths[idx]] = torch.Tensor([word.elmo[: self.elmoDim] for word in batchUtts[idx].tokens])
            if self.useGpu:
                wordSeqTensor = wordSeqTensor.cuda()
                tagSeqTensor = tagSeqTensor.cuda()
                wordSeqLengths = wordSeqLengths.cuda()
                elmoSeqTensor = elmoSeqTensor.cuda()
            inputBatches.append((wordSeqTensor, tagSeqTensor, wordSeqLengths, elmoSeqTensor))

        return inputBatches

    def getRawSentenceBatches(self, corpus, corpusMeta, batchSize):
        rawSentenceBatches = []
        totalSize = len(corpus.utterances)
        for batchId in range(totalSize // batchSize):
            batchUtts = corpus.utterances[batchId * batchSize: (batchId + 1) * batchSize]
            batchUtts = sorted(batchUtts, key=lambda utt: len(utt.tokens), reverse=True)
            sentences = []
            for idx in range(batchSize):
                sentences.append([word.rawWord for word in batchUtts[idx].tokens])
            rawSentenceBatches.append(sentences)
        if len(rawSentenceBatches) * batchSize < totalSize:
            startId = len(rawSentenceBatches) * batchSize
            lastBatchSize = totalSize - startId
            batchUtts = corpus.utterances[startId: totalSize]
            batchUtts = sorted(batchUtts, key=lambda utt: len(utt.tokens), reverse=True)
            sentences = []
            for idx in range(lastBatchSize):
                sentences.append([word.rawWord for word in batchUtts[idx].tokens])
            rawSentenceBatches.append(sentences)
        return rawSentenceBatches

    def negLogLikelihoodLoss(self, batchInput):
        wordSeqTensor, tagSeqTensor, wordSeqLengths, elmoSeqTensor = batchInput
        batchSize = wordSeqTensor.shape[0]
        sentLength = wordSeqTensor.shape[1]
        maskTemp = torch.range(1, sentLength, dtype=torch.int64).view(1, sentLength).expand(batchSize, sentLength)
        if self.useGpu:
            maskTemp = maskTemp.cuda()
        mask = torch.le(maskTemp, wordSeqLengths.view(batchSize, 1).expand(batchSize, sentLength))
        if self.useGpu:
            mask = mask.cuda()
        wordSeqEmbedding = self.wordEmbedding(wordSeqTensor, elmoSeqTensor)
        wordFeatures = self.encoder(wordSeqEmbedding, wordSeqLengths, True)
        totalScore, scores = self.crf(wordFeatures, wordSeqLengths, mask)
        goldScore = self.crf.scoreSentence(tagSeqTensor, wordSeqLengths, scores, mask)

        return totalScore - goldScore

    def forward(self, batchInput):
        wordSeqTensor, tagSeqTensor, wordSeqLengths, elmoSeqTensor = batchInput
        wordSeqEmbedding = self.wordEmbedding(wordSeqTensor, elmoSeqTensor)
        wordFeatures = self.encoder(wordSeqEmbedding, wordSeqLengths, False)
        bestScores, decodeIdx = self.crf.viterbiDecode(wordFeatures, wordSeqLengths)
        return bestScores, decodeIdx