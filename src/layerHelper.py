from src.models.baseline import Baseline
import torch.optim as optim
import torch
import os
from torch import nn
from src.models.encoders.lstmPlusLinear import LstmPlusLinear
from src.models.crfs.linearChainCRF import LinearChainCRF
from src.models.encoders.simpleEmbedding import SimpleEmbedding
from src.models.encoders.elmoEmbedding import ElmoEmbedding
from src.models.crfs.linearChainCRFDict import LinearChainCRFDict
from src.models.encoders.charWordEmbedding import CharWordEmbedding

class LayerUtils:
    def __init__(self, Config, corpusMeta):
        self.Config = Config
        self.tagSize = len(corpusMeta.tag2Idx)
        self.wordSize = len(corpusMeta.word2idx)
        self.wordEmbedding = corpusMeta.wordEmbedding
        self.charSize = len(corpusMeta.char2Idx)
        self.charEmbeddingDim = corpusMeta.charEmbeddingDim
        self.wordEmbeddingDim = self.getWordEmbeddingDim()

    def getWordEmbeddingDim(self):
        if self.Config.model.use_char:
            return self.Config.model.word_embedding_dim + self.Config.model.char_dim
        else:
            return self.Config.model.word_embedding_dim

    def getInitHidden(self, rnnType, numLayers, useBiRNN, batchSize, hiddenDim):
        if self.Config.train.get('rnn_hidden_initalization', 'none') == "none":
            hidden = None
        elif self.Config.train.get('rnn_hidden_initalization', 'none') == "randn":
            if useBiRNN:
                hiddenDim = hiddenDim // 2
            if rnnType == "lstm":
                hidden = (torch.randn(numLayers * 2 if useBiRNN else numLayers, batchSize, hiddenDim),
                        torch.randn(numLayers * 2 if useBiRNN else numLayers, batchSize, hiddenDim))
                if self.Config.use_gpu:
                    hidden = [item.cuda() for item in hidden]
            elif rnnType == "gru":
                hidden = torch.randn(numLayers * 2 if useBiRNN else numLayers, batchSize, hiddenDim)
                if self.Config.use_gpu:
                    hidden = hidden.cuda()
            else:
                print("Invalid rnn type {}".format(rnnType))
                exit(1)
            if self.Config.use_gpu:
                hidden = [item.cuda() for item in hidden]
        elif self.Config.train.get('rnn_hidden_initalization', 'none') == "normal":
            if useBiRNN:
                hiddenDim = hiddenDim // 2
            if rnnType == "lstm":
                hidden = (torch.normal(mean=torch.zeros(numLayers * 2 if useBiRNN else numLayers, batchSize, hiddenDim)),
                          torch.normal(mean=torch.zeros(numLayers * 2 if useBiRNN else numLayers, batchSize, hiddenDim)))
                if self.Config.use_gpu:
                    hidden = [item.cuda() for item in hidden]
            elif rnnType == "gru":
                hidden = torch.normal(mean=torch.zeros(numLayers * 2 if useBiRNN else numLayers, batchSize, hiddenDim))
                if self.Config.use_gpu:
                    hidden = hidden.cuda()
            else:
                print("Invalid rnn type {}".format(rnnType))
                exit(1)

        return hidden

    def getRNN(self, rnnType, useBiRNN, inputDim, hiddenDim, numLayers, dropout):
        if useBiRNN:
            hiddenDim = hiddenDim // 2
        if rnnType == "lstm":
            return nn.LSTM(inputDim, hiddenDim, numLayers, bidirectional=useBiRNN, dropout=dropout)
        elif rnnType == "gru":
            return nn.GRU(inputDim, hiddenDim, numLayers, bidirectional=useBiRNN, dropout=dropout)
        else:
            print("Invalide rnn type {}".format(rnnType))
            exit(1)

    def getEncoderRNN(self):
        if self.Config.model.model_type in ["baseline", 'baselineDict']:
            return (self.getRNN(self.Config.model.rnn_type, self.Config.model.use_birnn,
                                self.wordEmbeddingDim, self.Config.model.rnn_hidden_size,
                               self.Config.model.rnn_num_layers, self.Config.model.rnn_drop_out),
                    lambda currentBatchSize: self.getInitHidden(self.Config.model.rnn_type, self.Config.model.rnn_num_layers,
                                                                self.Config.model.use_birnn, currentBatchSize,
                                                                self.Config.model.rnn_hidden_size))
        elif self.Config.model.model_type == "baselineElmo":
            if self.Config.model.elmo_use_word_embedding:
                rawWordEmbeddingDim = self.Config.model.word_embedding_dim
            else:
                rawWordEmbeddingDim = 0
            if self.Config.model.use_linear_after_cat:
                wordEmbeddingDim = self.Config.model.word_embedding_dim
            else:
                if self.Config.model.use_elmo_linear:
                    wordEmbeddingDim = self.Config.model.word_embedding_dim + rawWordEmbeddingDim
                else:
                    wordEmbeddingDim = rawWordEmbeddingDim + self.Config.model.elmo_dim
            return (self.getRNN(self.Config.model.rnn_type, self.Config.model.use_birnn, wordEmbeddingDim, self.Config.model.rnn_hidden_size, self.Config.model.rnn_num_layers, self.Config.model.drop_out),
                    lambda currentBatchSize: self.getInitHidden(self.Config.model.rnn_type, self.Config.model.rnn_num_layers,
                                                                self.Config.model.use_birnn, currentBatchSize,
                                                                self.Config.model.rnn_hidden_size))

    def getEncoderLinear(self):
        if self.Config.model.model_type in ["baseline", "baselineElmo", 'baselineDict']:
            return nn.Linear(self.Config.model.rnn_hidden_size, self.tagSize)

    def getActivation(self):
        if self.Config.model.get('activation', 'none') == "none":
            return lambda x: x
        elif self.Config.model.get('activation', 'none') == "relu":
            return torch.nn.ReLU()
        elif self.Config.model.get('activation', 'none') == "tanh":
            return torch.nn.Tanh()
        elif self.Config.model.get('activation', 'none') == "sigmoid":
            return torch.nn.Sigmoid()
        else:
            print("Invalide activation type {}".format(self.Config.model.get('activation', 'none') ))
            exit(1)

    def getDropOut(self):
        return nn.Dropout(self.Config.model.drop_out)

    def getTransitionMatrix(self):
        if self.Config.train.get('transition_matrix_initalization', 'randn') == 'randn':
            transitions = nn.Parameter(torch.randn(self.tagSize, self.tagSize))
        elif self.Config.train.get('transition_matrix_initalization', 'randn') == 'normal':
            transitions = nn.Parameter(torch.normal(mean=torch.zeros(self.tagSize, self.tagSize)))
        transitions.data[self.Config.data.TAG_START_ID, :] = -10000
        transitions.data[:, self.Config.data.TAG_EOS_ID] = -10000
        transitions.data[:, 0] = -10000
        transitions.data[0, :] = -10000
        return transitions

    def getInitAlpha(self, batchSize):
        initAlpha = torch.full((batchSize, self.tagSize), -10000)
        for idx in range(batchSize):
            initAlpha[idx][self.Config.data.TAG_START_ID] = 0
        if self.Config.use_gpu:
            initAlpha = initAlpha.cuda()
        return initAlpha

    def getInitAlphaVector(self):
        initAlpha = torch.full((self.tagSize), -10000)
        initAlpha[self.Config.data.TAG_START_ID] = 0
        if self.Config.use_gpu:
            initAlpha = initAlpha.cuda()
        return initAlpha

    def getInitAlphaWithBatchSize(self):
        return lambda batchSize: self.getInitAlpha(batchSize)

    def getTagSize(self):
        return self.tagSize

    def getTagEndId(self):
        return self.Config.data.TAG_EOS_ID

    def getTagStartId(self):
        return self.Config.data.TAG_START_ID

    def getUseGpu(self):
        return self.Config.use_gpu

    def getEmbeddingParameter(self):
        embedding = nn.Embedding.from_pretrained(torch.FloatTensor(self.wordEmbedding), freeze=False)
        return embedding

    def getCharEmbeddingParameter(self):
        return nn.Embedding(self.charSize, self.charEmbeddingDim)

    def getCharConv(self):
        return nn.Conv1d(self.charEmbeddingDim, self.Config.model.char_dim, kernel_size=3, padding=1)

    def getCharRNN(self):
        return nn.LSTM(self.charEmbeddingDim, self.Config.model.char_dim, bidirectional=False)

    def getElmoLinear(self):
        if self.Config.model.use_elmo_linear:
            return nn.Linear(self.Config.model.elmo_dim, self.Config.model.word_embedding_dim)
        else:
            return None

    def getLinearAfterCat(self):
        if self.Config.model.model_type in ["baselineElmo"]:
            if self.Config.model.elmo_use_word_embedding:
                rawWordEmbeddingDim = self.Config.model.word_embedding_dim
            else:
                rawWordEmbeddingDim = 0
            if self.Config.model.use_linear_after_cat:
                if self.Config.model.use_elmo_linear:
                    return nn.Linear(self.Config.model.word_embedding_dim + rawWordEmbeddingDim, self.Config.model.word_embedding_dim)
                else:
                    return nn.Linear(rawWordEmbeddingDim + self.Config.model.elmo_dim, self.Config.model.word_embedding_dim)
            else:
                return None

class LayerHelper:
    def __init__(self, Config, layerUtils):
        self.Config = Config
        self.layerUtils = layerUtils

    def getEncoder(self):
        if self.Config.model.model_type in ["baseline", "baselineElmo", 'baselineDict']:
            return LstmPlusLinear(self.layerUtils)

    def getCRF(self):
        if self.Config.model.model_type in ["baseline", "baselineElmo"]:
            return LinearChainCRF(self.layerUtils)
        if self.Config.model.model_type in ['baselineDict']:
            return LinearChainCRFDict(self.layerUtils)

    def getWordEmbedding(self):
        if self.Config.model.model_type in ["baseline", 'baselineDict']:
            if self.Config.model.use_char:
                return CharWordEmbedding(self.layerUtils)
            else:
                return SimpleEmbedding(self.layerUtils)
        elif self.Config.model.model_type in ["baselineElmo"]:
            return ElmoEmbedding(self.layerUtils)

    def getElmoLinear(self):
        return self.layerUtils.getElmoLinear()
