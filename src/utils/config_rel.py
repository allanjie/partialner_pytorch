# 
# @author: Allan
#

def read_conf_file(file):
    f = open(file, 'r', encoding='utf-8')
    arguments = []
    for line in f:
        line = line.rstrip('\n').rstrip('\r')
        if line == "" or line.startswith('#'):
            continue
        arg, value = line.split(': ')
        arguments.append('--' + arg)
        arguments.append(value)
    f.close()
    return arguments